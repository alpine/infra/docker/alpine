#!/bin/sh

case $1 in
    x86_64) echo linux/amd64;;
    x86) echo linux/i386;;
    aarch64) echo linux/arm64/v8;;
    armv7) echo linux/arm/v7;;
    armhf) echo linux/arm/v6;;
    ppc64le) echo linux/ppc64le;;
    s390x) echo linux/s390x;;
    riscv64) echo linux/riscv64;;
esac
